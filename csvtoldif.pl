#!/usr/bin/perl

use warnings;
use strict;
use POSIX;
use Net::LDAP;
use Net::LDAPS;
use Net::LDAP::LDIF;
use Text::CSV;
use Getopt::Long;
use File::Glob ':glob';
use Data::Dumper;
use Carp;


my @test = (1,2,3);

# The CSV file(s) specified on the command line that tell us which
# users to queue.
#my @CSVFILES = @ARGV;

my @CSVFILES = @ARGV;

foreach my $csvfile ( @CSVFILES ) {
  print "    Processing CSV file $csvfile for LDIF import.\n";
  my @createusers = getUsersToLoad( $csvfile );

  foreach my $user ( @createusers ) {
    #eval { write_ldif( $user ) };
    #if( $@ ) {
    #carp "Error queueing user $user->{uid}: $@";
    print Dumper($user);
  }
}


sub getUsersToLoad {
  my( $csvFile ) = @_;
  croak "CSV file $csvFile does not exist" if ( ! -e $csvFile );

  my $csv = Text::CSV->new();

  my @createusers = ();

  open( my $CSVFH, "<", $csvFile ) or die "Could not open $csvFile: $!\n";
  while( my $user = <$CSVFH> ) {
    next if $user =~ /^(\s)*$/;  # skip blank lines

    if( $csv->parse( $user ) ) {
      my @fields = map { my $a = $_; $a =~ s/^\s+//; $a } $csv->fields;

      my %thisUser = (
        givenname => $fields[0],
        sn        => $fields[1]
      );

      my $derivedName =
        lc( substr($thisUser{givenname}, 0, 1) . $thisUser{sn});

      $thisUser{uid} = defined $fields[2] ? $fields[2] : $derivedName;

      $thisUser{mail} = $fields[3];
      my @group_items = split(' ',$fields[4]);
      print 'bws',@group_items;
      $thisUser{groups} = \@group_items;
      #$thisUser{clearpass} = generate_random_pw_quadchar(1) . generate_varchar(4);
      #my $hashedstr =  Crypt::SaltedHash->new(algorithm => 'SHA-1');
      #$hashedstr->add( $thisUser{clearpass} );
      #$thisUser{hashedpass} = $hashedstr->generate;
      push @createusers, \%thisUser;
    }
  }
  close $CSVFH;

  return @createusers;
}


print join(", ",@ARGV);
